/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

function dodajSlike(vhodnoBesedilo) {
  if (vhodnoBesedilo.indexOf('/zasebno') == 0) {
    var add = "", temp = vhodnoBesedilo;
    temp = temp.split(" ");
    for (var i = 2; i < temp.length; i++) {
      if (temp[i].indexOf("http://") > -1) {
        if (temp[i].indexOf(".jpg") > -1 || temp[i].indexOf(".png") > -1 || temp[i].indexOf(".gif") > -1) {
          if (temp[i].indexOf("http://") > 0) {
            if (!((temp[i][temp[i].indexOf("http://")-1] >= 'a' && temp[i][temp[i].indexOf("http://")-1] <= 'z')
            || (temp[i][temp[i].indexOf("http://")-1] >= 'A' && temp[i][temp[i].indexOf("http://")-1] <= 'Z')
            || (temp[i][temp[i].indexOf("http://")-1] >= '0' && temp[i][temp[i].indexOf("http://")-1] <= '9'))) {
              add += " <br/><img style = 'width: 200px; padding-left: 20px;' src = '" + temp[i].substring(temp[i].indexOf("http://"),
              Math.min((temp[i].indexOf(".jpg") > -1 ? temp[i].indexOf(".jpg") : 999999),
              Math.min((temp[i].indexOf(".png") > -1 ? temp[i].indexOf(".png") : 999999),
                       (temp[i].indexOf(".gif") > -1 ? temp[i].indexOf(".gif") : 999999))) + 4) + "' />";
            }
          } else {
            add += " <br/><img style = 'width: 200px; padding-left: 20px;' src = '" + temp[i].substring(temp[i].indexOf("http://"),
              Math.min((temp[i].indexOf(".jpg") > -1 ? temp[i].indexOf(".jpg") : 999999),
              Math.min((temp[i].indexOf(".png") > -1 ? temp[i].indexOf(".png") : 999999),
                       (temp[i].indexOf(".gif") > -1 ? temp[i].indexOf(".gif") : 999999))) + 4) + "' />";
          }
        }
      }
    }
    vhodnoBesedilo = vhodnoBesedilo.substr(0, vhodnoBesedilo.length - 1);
    vhodnoBesedilo += '<br/>' + add;
  } else {
    var add = "", temp = vhodnoBesedilo;
    temp = temp.split(" ");
    for (var i = 0; i < temp.length; i++) {
      if (temp[i].indexOf("http://") > -1) {
        if (temp[i].indexOf(".jpg") > -1 || temp[i].indexOf(".png") > -1 || temp[i].indexOf(".gif") > -1) {
          if (temp[i].indexOf("http://") > 0) {
            if (!((temp[i][temp[i].indexOf("http://")-1] >= 'a' && temp[i][temp[i].indexOf("http://")-1] <= 'z')
            || (temp[i][temp[i].indexOf("http://")-1] >= 'A' && temp[i][temp[i].indexOf("http://")-1] <= 'Z')
            || (temp[i][temp[i].indexOf("http://")-1] >= '0' && temp[i][temp[i].indexOf("http://")-1] <= '9'))) {
              add += " <br/><img style = 'width: 200px; padding-left: 20px;' src = '" + temp[i].substring(temp[i].indexOf("http://"),
              Math.min((temp[i].indexOf(".jpg") > -1 ? temp[i].indexOf(".jpg") : 999999),
              Math.min((temp[i].indexOf(".png") > -1 ? temp[i].indexOf(".png") : 999999),
                       (temp[i].indexOf(".gif") > -1 ? temp[i].indexOf(".gif") : 999999))) + 4) + "' />";
            }
          } else {
            add += " <br/><img style = 'width: 200px; padding-left: 20px;' src = '" + temp[i] + "' />";
          }
        }
      }
    }
    vhodnoBesedilo += add;
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else  if (sporocilo.indexOf("&#9756;") > -1) {
   return $('<div style="font-weight: bold"></div>').html(sporocilo);
  } else if (sporocilo.indexOf("http://") > -1 &&
             (sporocilo.indexOf(".jpg") || sporocilo.indexOf(".png") || sporocilo.indexOf(".gif"))) {
    sporocilo = 
      sporocilo.split("<br/>").join("")
               .split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("jpg' /&gt;").join("jpg' />")
               .split("png' /&gt;").join("png' />")
               .split("gif' /&gt;").join("gif' />");
    var temp = sporocilo;
    sporocilo = "";
    while (temp.length > 0) {
      if (temp.indexOf("<img") > 0) {
        sporocilo += '<div style="font-weight: bold">' + temp.substring(0, temp.indexOf("<img")) + '</div>';
        temp = temp.substring(temp.indexOf("<img"), temp.length);
      } else if (temp.indexOf("<img") == 0) {
        sporocilo += temp.substring(0, temp.indexOf("/>") + 2);
        temp = temp.substring(temp.indexOf("/>") + 2, temp.length);
      } else {
        sporocilo += '<div style="font-weight: bold">' + temp + '</div>';
        break;
      }
    }
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  sporocilo = dodajSlike(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";
var normalnoIme = [];
var nadimki = [];

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  socket.on('spremembaVzdevekUporabnika', function(rezultat) {
    for (var i = 0; i < normalnoIme.length; i++) {
      if (rezultat.stariVzdevek == normalnoIme[i]) {
        normalnoIme[i] = rezultat.noviVzdevek;
      }
    }
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    for (var i = 0; i < normalnoIme.length; i++) {
      if (sporocilo.besedilo.indexOf(normalnoIme[i]) == 0) {
        sporocilo.besedilo = nadimki[i] + ' (' + normalnoIme[i] + ')' + sporocilo.besedilo.substring(normalnoIme[i].length, sporocilo.besedilo.length); break;
      }
    }
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('zasebnoOdgovor', function(rezultat) {
    $("#sporocila").append(divElementEnostavniTekst(rezultat.besedilo));
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      var found = -1;
      for (var j = 0; j < normalnoIme.length; j++) {
        if (normalnoIme[j] == uporabniki[i]) {
          found = j; break;
        }
      }
      if (found == -1) {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      } else {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(nadimki[found] + ' (' + uporabniki[i] + ')'));
      }
    }
    
    $('#seznam-uporabnikov div').click(function() {
      if ($(this).text() != trenutniVzdevek) {
        for (var i = 0; i < nadimki.length; i++) {
          if ($(this).text() == nadimki[i] + ' (' + normalnoIme[i] + ')') {
            $('#sporocila').append(divElementHtmlTekst('<b> (zasebno za ' + $(this).text() + '): &#9756;'));
          }
        }
        klepetApp.procesirajUkaz('/zasebno "' + $(this).text() + '" "&#9756;"');
        $('#poslji-sporocilo').focus();
      } else {
        $('#sporocila').append(divElementEnostavniTekst('Ne morete krcnite sami sebe!'));
      }
    });
  });
  
  socket.on('preimenujUporabnika', function(rezultat) {
    if (!rezultat.uspesno) {
      $('#sporocila').append(divElementEnostavniTekst('Ne morete preimenuti ta uporabnika kjer ta nadimek je ze v uporabi kot vzdevek drugega uporabnika!'));
    } else if (rezultat.uporabnik == trenutniVzdevek) {
      $('#sporocila').append(divElementEnostavniTekst('Uporabite /vzdevek [vzdevek] da spremenite svoji vzdevek!'));
    } else {
      $('#sporocila').append(divElementEnostavniTekst('Uporabnik je uspesno preimenovan'));
      var found = false;
      for (var i = 0; i < nadimki.length; i++) {
        if (nadimki[i] == rezultat.uporabnik) {
          found = true;
          nadimki[i] = rezultat.nadimek;
        }
      }
      if (!found) {
        for (var i = 0; i < normalnoIme.length; i++) {
          if (rezultat.uporabnik == normalnoIme[i]) {
            found = true;
            nadimki[i] = rezultat.nadimek;
          }
        }
        if (!found) {
          normalnoIme.push(rezultat.uporabnik);
          nadimki.push(rezultat.nadimek);
        }
      }
    }
  });
  
  socket.on('updatePriemeovanja', function(rezultat) {
    for (var i = 0; i < nadimki.length; i++) {
      if (rezultat.nadimek == nadimki[i]) {
        $('#sporocila').append(divElementEnostavniTekst('Uporabnik ' + normalnoIme[i] + ' (' + nadimki[i] + ') ni vec preimenovan kjer drug uporabnik uporablja ta nadimek kot vzdevek'));
        delete nadimki[i];
        delete normalnoIme[i];
      }
    }
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});